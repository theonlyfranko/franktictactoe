#pragma once

#include <iostream>
#include <conio.h>

using namespace std;

class TicTacToe
{

private:
					//   0    1    2    3    4    5    6    7    8
	char m_board[9] = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' };
	int m_numTurns = 1;
	char m_playerTurn;
	int position;
	int validMoves[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };


	void PrintColumns(char x, char y, char z)
	{
		cout << " " << x << " | " << y << " | " << z << " " << endl;
	}

	void PrintRows()
	{
		cout << "---+---+---" << endl;
	}

public:

	// Displays the board and all current moves made on the board.
	void DisplayBoard()
	{

		PrintColumns(m_board[0], m_board[1], m_board[2]);
		PrintRows();
		PrintColumns(m_board[3], m_board[4], m_board[5]);
		PrintRows();
		PrintColumns(m_board[6], m_board[7], m_board[8]);
		cout << endl << endl;
	}

	// Was having a problem with displaying X when I meant to display O and vice-versa. This fixes that.
	void fixWinner()
	{
		m_numTurns = m_numTurns + 1;
		GetPlayerTurn();
	}

	// this checks all possible win combinations and if either player has met any of these combinations yet.
	bool IsOver()
	{
		while ((validMoves[0] + validMoves[1] + validMoves[2] + validMoves[3] + validMoves[4] + validMoves[5] + validMoves[6] + validMoves[7] + validMoves[8] < 45))
		{
			// Top row
			if (m_board[0] != ' ' && m_board[0] == m_board[1] && m_board[0] == m_board[2])
			{
				return true;
			}
			// Middle row
			else if (m_board[3] != ' ' && m_board[3] == m_board[4] && m_board[3] == m_board[5])
			{
				return true;
			}
			// Bottom row
			else if (m_board[6] != ' ' && m_board[6] == m_board[7] && m_board[6] == m_board[8])
			{
				return true;
			}
			// Left column
			else if (m_board[0] != ' ' && m_board[0] == m_board[3] && m_board[0] == m_board[6])
			{
				return true;
			}
			// Center column
			else if (m_board[1] != ' ' && m_board[1] == m_board[4] && m_board[1] == m_board[7])
			{
				return true;
			}
			// Right column
			else if (m_board[2] != ' ' && m_board[2] == m_board[5] && m_board[2] == m_board[8])
			{
				return true;
			}
			// Diagonally \ 
			else if (m_board[0] != ' ' && m_board[0] == m_board[4] && m_board[0] == m_board[8])
			{
				return true;
			}
			// Diagonally /
			else if (m_board[2] != ' ' && m_board[2] == m_board[4] && m_board[2] == m_board[6])
			{
				return true;
			}
			{
				return false;
			}
		}
	}

	// Figure's out whose turn it is.
	char GetPlayerTurn()
	{
		if (m_numTurns % 2 == 0)
		{
			return 'O';
		}
		else
		{
			return 'X';
		}
	}

	// checks to see if the move entered has not been entered already.
	bool IsValidMove(int position)
	{
		if (position != validMoves[position - 1])
		{
			return true;
		}
		else
		{
			cout << "That is an invalid move. Please select a different position." << endl << endl;
			return false;
		}
	}

	// inputs the player's move into the validMoves array and flips which player's turn to display
	void Move(int position)
	{
		m_board[position - 1] = GetPlayerTurn();
		m_numTurns++;
		validMoves[position - 1] = position;
	}

	// Announce's if there is a winner, or if the game ended in a tie.
	void DisplayResult()
	{
		if (IsOver() == true)
		{
			fixWinner();
			cout << "Player " << GetPlayerTurn() << " is the winner!" << endl << endl << endl;
		}
		else
		{
			cout << "It's a tie" << endl << endl << endl;
		}
	}
};